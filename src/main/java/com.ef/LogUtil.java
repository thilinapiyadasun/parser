package com.ef;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class LogUtil {

    static String PATH = "--accesslog";
    static String START_DATE = "--startDate";
    static String DURATION = "--duration";
    static String THRESHOLD = "--threshold";
    static String DAILY = "daily";
    static String HOURLY = "hourly";

    static Timestamp parseToTimeStamp(String logDate) {

        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss.SSS");
            DateTime dt = formatter.parseDateTime(logDate);
            Timestamp timeStamp = new Timestamp(dt.getMillis());
            return timeStamp;
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    private static java.sql.Date convertUtilToSql(java.util.Date uDate) {
        java.sql.Date sDate = new java.sql.Date(uDate.getTime());
        return sDate;
    }

    static Time parseTime(String s) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            long ms = sdf.parse(s).getTime();
            Time t = new Time(ms);
            return t;
        } catch (Exception e) {

            e.printStackTrace();
        }
        return null;
    }

    static Timestamp parseTimeShort(String str) {
        try {
            DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
            DateTime dt = formatter.parseDateTime(str);
            Timestamp timeStamp = new Timestamp(dt.getMillis());
            return timeStamp;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}


package com.ef;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectToDB {

    private static ConnectToDB singleton;

    /*
     * A private Constructor prevents any other class from instantiating.
     */
    private ConnectToDB() {
    }

    /* Static 'instance' method */
    public static ConnectToDB getInstance() {

        if (singleton == null) {
            synchronized (ConnectToDB.class) {
                if (singleton == null) {
                    singleton = new ConnectToDB();
                }
            }
        }
        return singleton;
    }

    /**
     * @return
     */
    public Connection accessDataBase() {

        String dri = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://remotemysql.com:3306/R03VvyfFKw?rewriteBatchedStatements=true";
        try {
            Class.forName(dri);
            Connection con = DriverManager.getConnection(url, "R03VvyfFKw", "FBc42MKoxo");//Connect to the database
            return con;

        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return null;

    }
}

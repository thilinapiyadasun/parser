package com.ef;

import java.sql.Timestamp;
import java.util.Calendar;

public class Parser {

    private String path;
    private Timestamp logDateTime;
    private int duration; // 1 will be for hourly,2 will be for daily
    private int threshold;
    QueryProcessor queryProcessor;

    public static void main(String args[]) {

        Parser parser = new Parser();

        System.out.println("------------------------------------------------------------");
        System.out.println("              Reset table and delete current records");
        System.out.println("------------------------------------------------------------");
        parser.resetTable();

        System.out.println("------------------------------------------------------------");
        System.out.println("                      Extracting inputs");
        System.out.println("------------------------------------------------------------");

        for (String arg : args) {
            parser.extractInputs(arg);
        }

        System.out.println("------------------------------------------------------------");
        System.out.println("                      Reading Access.log");
        System.out.println("          will be completed with 11 batch iterations");
        System.out.println("------------------------------------------------------------");
        parser.readFile();

        System.out.println("------------------------------------------------------------");
        System.out.println("                      Create Output");
        System.out.println("------------------------------------------------------------");
        parser.queryLogData();
    }

    /**
     * reset access.log SQL table data
     */
    private void resetTable(){
        try {
            queryProcessor = QueryProcessor.getInstance();
            queryProcessor.resetData();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * extract user entered inputs and initialize parameters
     *
     * @param arg
     */
    private void extractInputs(String arg) {

        String args[] = arg.split("=");
        if (args[0].equals(LogUtil.PATH)) {
            this.path = args[1];
        } else if (args[0].equals(LogUtil.START_DATE)) {

            String timeStr = args[1];
            System.out.println("Date and Time   : "+ timeStr);
            String dateAndTime = timeStr.replace(".", " ");
            this.logDateTime = LogUtil.parseTimeShort(dateAndTime);

        } else if (args[0].equals(LogUtil.DURATION)) {
            System.out.println("Duration        : "+ args[1]);
            if (args[1].equals(LogUtil.DAILY)) {
                this.duration = 2;
            }
            if (args[1].equals(LogUtil.HOURLY)) {
                this.duration = 1;
            }
        } else if (args[0].equals(LogUtil.THRESHOLD)) {
            System.out.println("Threshold       : "+ args[1]);
            this.threshold = Integer.parseInt(args[1]);
        }
    }

    /**
     * read access.log
     */
    public void readFile() {

        try {
            queryProcessor = QueryProcessor.getInstance();
            if (this.path != null) {
                queryProcessor.read(this.path);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * create results
     */
    public void queryLogData() {

        try {
            queryProcessor = QueryProcessor.getInstance();
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(this.logDateTime.getTime());
            if (this.duration == 1) {
                cal.add(Calendar.HOUR, 1);
            } else if (this.duration == 2) {
                cal.add(Calendar.HOUR, 24);
            }
            Timestamp endDateTime = new Timestamp(cal.getTime().getTime());
            queryProcessor.queryLogData(this.logDateTime, endDateTime, this.threshold);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
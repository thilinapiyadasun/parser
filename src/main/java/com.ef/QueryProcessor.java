package com.ef;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.*;

public class QueryProcessor {

    private static QueryProcessor queryProcessor;

    private QueryProcessor() {
    }

    /**
     * create single QueryProcessor object
     *
     * @return
     */
    public static QueryProcessor getInstance() {

        if (queryProcessor == null) {
            synchronized (ConnectToDB.class) {
                if (queryProcessor == null) {
                    queryProcessor = new QueryProcessor();
                }
            }
        }
        return queryProcessor;
    }

    /**
     * reset AccessLogData table and delete all records
     * @throws Exception
     */
    public void resetData() throws Exception{
        Connection con = ConnectToDB.getInstance().accessDataBase();
        StringBuffer hourlyQuery = new StringBuffer("Delete from AccessLogData");
        Statement st = con.createStatement();
        st.executeUpdate(hourlyQuery.toString());
    }

    /**
     * read file and insert log data into MySql Database
     *
     * @param path
     * @throws Exception
     */
    public void read(String path) throws Exception {

        FileInputStream fstream = new FileInputStream(path);
        BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
        String strLine;
        String sql = "insert into AccessLogData ( logTime, ip, status, message) values ( ?, ?, ? , ? )";
        Connection connection = ConnectToDB.getInstance().accessDataBase();
        PreparedStatement ps = connection.prepareStatement(sql);
        final int batchSize = 10000;
        int count = 0;

        while ((strLine = br.readLine()) != null) {
            int parmCount = 1;
            String logs[] = strLine.split("\\|");

            ps.setTimestamp(parmCount++, LogUtil.parseToTimeStamp(logs[0]));
            ps.setString(parmCount++, logs[1]);
            ps.setInt(parmCount++, Integer.parseInt(logs[3]));
            ps.setString(parmCount++, logs[4]);
            ps.addBatch();
            if (++count % batchSize == 0) {
                ps.executeBatch();
                System.out.println("Batch execution Success");
            }
        }
        ps.executeBatch(); // insert remaining records
        ps.close();
        connection.close();
        fstream.close();

    }

    /**
     * Query database for hourly and daily threshold exceeded logs
     *
     * @param start
     * @param end
     * @param threshold
     * @throws Exception
     */
    public void queryLogData(Timestamp start, Timestamp end, int threshold) throws Exception {

        Connection con = ConnectToDB.getInstance().accessDataBase();
        StringBuffer hourlyQuery = new StringBuffer("SELECT ip , count(*) as count" +
                " FROM AccessLogData WHERE ");
        hourlyQuery.append("logTime >= ? AND ");
        hourlyQuery.append("logTime < ? ");
        hourlyQuery.append("GROUP BY ip HAVING count > ?");

        PreparedStatement ps = con.prepareStatement(hourlyQuery.toString());
        int count = 1;
        ps.setTimestamp(count++, start);
        ps.setTimestamp(count++, end);
        ps.setInt(count++, threshold);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            this.load(rs);
        }
        rs.close();
        ps.close();
    }

    /**
     * load and sout results
     *
     * @param rs
     * @throws SQLException
     */
    public void load(ResultSet rs) throws SQLException {
        String ip = rs.getString("ip");
        System.out.println(ip);
    }
}

